package com.example.miniblog.models.dtos;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data

public class PostDto {
    @NotEmpty
    @Size(min = 10, max = 8192, message = "Content should be between 10 and 8192 symbols")
    private String post;
}
