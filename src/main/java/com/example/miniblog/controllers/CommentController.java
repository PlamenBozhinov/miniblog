package com.example.miniblog.controllers;

import com.example.miniblog.exceptions.DuplicateEntityException;
import com.example.miniblog.exceptions.EntityNotFoundException;
import com.example.miniblog.models.Comment;
import com.example.miniblog.models.Post;
import com.example.miniblog.models.dtos.CommentDto;
import com.example.miniblog.services.contracts.CommentService;
import com.example.miniblog.services.contracts.PostService;
import com.example.miniblog.services.mappers.CommentMapper;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.NoSuchElementException;

@RestController
@AllArgsConstructor
@RequestMapping("/api/comments")
public class CommentController {

    private final CommentService commentService;
    private final CommentMapper commentMapper;
    private final PostService postService;

    @PostMapping("/api/posts/{id}/comment")
    public Comment create(@PathVariable int id, @Valid @RequestBody CommentDto commentDto) {
        try {
            Post post = postService.get(id);

            Comment comment = commentMapper.dtoToObject(commentDto);
            comment.setPost(post);
            commentService.create(comment);
            return comment;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/api/comments/{id}")
    public Comment update(@PathVariable int id, @Valid @RequestBody CommentDto commentDto) {
        try {

            Comment comment = commentMapper.dtoToObject(id, commentDto);
            commentService.update(comment);
            return comment;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            Comment comment = commentService.get(id);
            commentService.delete(id);
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

}
