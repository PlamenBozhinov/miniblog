package com.example.miniblog.controllers;

import com.example.miniblog.models.Post;
import com.example.miniblog.models.dtos.PostDto;

import com.example.miniblog.services.contracts.PostService;
import com.example.miniblog.services.mappers.PostMapper;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@AllArgsConstructor
@RequestMapping("/api/posts")
public class PostController {
    private final PostService postService;
    private final PostMapper postDtoMapper;

    @GetMapping
    public List<Post> getAll() {
        return postService.getAll();
    }

    @PostMapping()
    public void createPost(@RequestBody @Valid PostDto postDto) {
        postService.create(postDtoMapper.dtoToObject(postDto));
    }

    @GetMapping("/{id}")
    public Post getPostById(@PathVariable int id) {
        try {
            return postService.get(id);
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            Post post = postService.get(id);
            postService.delete(id);
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Post update(@PathVariable int id, @RequestBody PostDto postDto) {
        try {
             Post post = postDtoMapper.dtoToObject(id, postDto);
            postService.update(post);
            return post;
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

}
