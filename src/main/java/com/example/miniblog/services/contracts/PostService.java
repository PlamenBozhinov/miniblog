package com.example.miniblog.services.contracts;

import com.example.miniblog.models.Post;
import java.util.List;

public interface PostService {
    Post get(int id);

    void create(Post post);

    void update(Post post);

    void delete(int id);

    List<Post> getAll();

}
