package com.example.miniblog.services.contracts;

import com.example.miniblog.models.Comment;

public interface CommentService {

    Comment get(int id);

    void create(Comment comment);

    void update(Comment comment);

    void delete(int id);

}
