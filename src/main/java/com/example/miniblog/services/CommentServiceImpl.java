package com.example.miniblog.services;

import com.example.miniblog.exceptions.EntityNotFoundException;
import com.example.miniblog.models.Comment;
import com.example.miniblog.repository.CommentRepository;
import com.example.miniblog.services.contracts.CommentService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CommentServiceImpl implements CommentService {
    private CommentRepository commentRepository;
    @Override
    public Comment get(int id) {
        Comment comment =  commentRepository.findById(id)
                .orElseThrow(()-> new EntityNotFoundException("No Comment found", id));
        return comment;
    }

    @Override
    public void create(Comment comment) {
        commentRepository.save(comment);
    }

    @Override
    public void update(Comment comment) {
        commentRepository.save(comment);
    }

    @Override
    public void delete(int id) {
        commentRepository.deleteById(id);
    }
}
