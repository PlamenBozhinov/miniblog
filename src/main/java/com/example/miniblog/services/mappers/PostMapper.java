package com.example.miniblog.services.mappers;

import com.example.miniblog.models.Post;
import com.example.miniblog.models.dtos.PostDto;
import com.example.miniblog.services.contracts.PostService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
@Component
@AllArgsConstructor
public class PostMapper {
    private final PostService postService;
    public Post dtoToObject(PostDto postDto){
        Post post = new Post();
        post.setContent(postDto.getPost());
        post.setDate(LocalDateTime.now());
        return post;
    }
    public Post dtoToObject(int id, PostDto dto) {
        Post post = dtoToObject(dto);
        post.setId(id);
        Post repositoryPost = postService.get(id);
        post.setComments(repositoryPost.getComments());
        return post;
    }



}
