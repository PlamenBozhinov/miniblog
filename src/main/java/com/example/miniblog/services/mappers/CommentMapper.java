package com.example.miniblog.services.mappers;

import com.example.miniblog.models.Comment;
import com.example.miniblog.models.dtos.CommentDto;
import com.example.miniblog.services.contracts.CommentService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@AllArgsConstructor
public class CommentMapper {

    private final CommentService commentService;
    public Comment dtoToObject(int id, CommentDto dto) {
        Comment comment = dtoToObject(dto);
        comment.setId(id);
        Comment repositoryComment = commentService.get(id);
        comment.setPost(repositoryComment.getPost());
        return comment;
    }

    public Comment dtoToObject(CommentDto commentDto) {
        Comment comment = new Comment();
        comment.setComment(commentDto.getComment());
        comment.setDate(LocalDateTime.now());
        return comment;
    }

}
