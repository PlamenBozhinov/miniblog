package com.example.miniblog.services;

import com.example.miniblog.exceptions.EntityNotFoundException;
import com.example.miniblog.models.Post;
import com.example.miniblog.repository.PostRepository;
import com.example.miniblog.services.contracts.PostService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
@AllArgsConstructor
public class PostServiceImpl implements PostService {
    private PostRepository postRepository;

    @Override
    public Post get(int id) {
        Post post = postRepository.findById(id).orElseThrow(()-> new EntityNotFoundException("Post not found", id));
        return post;
    }

    @Override
    public void create(Post post) {
        postRepository.save(post);
    }

    @Override
    public void update(Post post) {
        postRepository.save(post);
    }

    @Override
    public void delete(int id) {
        postRepository.deleteById(id);
    }

    @Override
    public List<Post> getAll() {
        return postRepository.findAll();
    }
}
